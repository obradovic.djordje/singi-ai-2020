# Podešavanje okruženja

Napraviti virtualno okruženje, npr.:

```
C:\>python -m venv singi-ai
```

Aktivirati okruženje:

```
C:\>singi-ai\Scripts\activate.bat
(singi-ai) C:\>
```

Instalirati biblioteku numpy, matplotlib i aplikaciju JupyterLab:

```
(singi-ai) C:\>python -m pip install -U pip
(singi-ai) C:\>python -m pip install numpy
(singi-ai) C:\>python -m pip install matplotlib
(singi-ai) C:\>python -m pip install jupyterlab
```

Deaktivirati orkuženje, ponovo ga aktivirati i pokrenuti JupyterLab:

```
(singi-ai) C:\>deactivate
C:\>singi-ai\Scripts\activate.bat
(singi-ai) C:\>jupyter-lab
```